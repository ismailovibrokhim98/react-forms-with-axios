import axios from "axios";
import React, { useState } from "react";
import FormInput from "../FormInput/FormInput";
import './form.css'
import MButton from "../MButton/MButton";

const Form = () => {
  const [loading, setLoading] = useState(false);
  const initialValues = {
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
  };
  const [values, setValues] = useState(initialValues);

  const inputs = [
    {
      name: "username",
      type: "text",
      errorMessage: "Username is required, must be at least 3 characters",
      label: "Username",
      value: values.username,
      required: true,
      pattern: "^[a-zA-Z0-9]{3,}$",
  
    },
    {
      name: "email",
      type: "email",
      errorMessage: "Email is required",
      label: "Email",
      value: values.email,
      required: true,
      pattern:
        "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$",
   
    },
    {
      name: "password",
      type: "password",
      errorMessage: "Password is required, must be at least 6 characters",
      label: "Password",
      value: values.password,
      required: true,
      pattern: "^[a-zA-Z0-9]{6,13}$",
  
    },
    {
      name: "confirmPassword",
      type: "password",
      errorMessage: "Confirm Password should match with Password",
      label: "Confirm Password",
      value: values.confirmPassword,
      required: true,
      pattern: values.password,
    },
  ];

  const onChange = (e) => {
    console.log('e.target.name', e.target.name);
    console.log('e.target.value', e.target.value);
    setValues({ ...values, [e.target.name]: e.target.value })
  }

  const onSubmit = (e) => {
    e.preventDefault();
    const formData = {
      ...values,
    };
    console.log("formData", formData);
    setLoading(true);
    axios
      .post("https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/user", formData)
      .then((res) => {
        setLoading(false);
        setValues(initialValues);
        // getUsers();
      });
  };

  return (
    <>
      <div className="formWrapper">
        <h2>Create User</h2>
        <form className="form" onSubmit={onSubmit}>
          {inputs.map((input) => (
            <FormInput
              key={input.name}
                {...input}
                onChange={onChange}
            />
          ))}
          
          <MButton
            BLoading={loading}
            BSize={"submit"}
            BStyle={{ marginBottom: "15px" }}
          >
            Сохранить
          </MButton>
        </form>
      </div>
    </>
  );
};

export default Form;
