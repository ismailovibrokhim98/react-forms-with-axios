import React from "react";
import { ReactComponent as DeleteIcon } from "../../Assets/Images/delete.svg";
import MButton from "../MButton/MButton";
import "./JobType.css";

const JobList = ({ jobs, deleteJob }) => {
  return (
    <div className="jobList">
      <h1>Job type</h1>
      <div className="line"></div>
      <div className="static-2 default-2">
      <div className="item-2">№</div>
        <div className="item-2">Label</div>
        <div className="item"></div>
      </div>
      {jobs &&
        jobs.map((job) => (
          <div className="default-2" key={job?.id}>
             <div className="item-2">{job?.id}</div>
            <div className="item-2">{job?.label}</div>
            
            <MButton
              onClick={() => {
                console.log('delete user',job?.id);
                deleteJob(job?.id)
              }}
              BClass="item-2 action-2 action__delete-2"
              BType={"outline-primary"}
              BIcon={<DeleteIcon />}
            />
          </div>
        ))}
    </div>
  );
};

export default JobList;
