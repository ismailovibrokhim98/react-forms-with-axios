import React, { useEffect, useState } from "react";
import "./App.css";
import axios from "axios";
import UserList from "./components/UserList/UserList";
import FormInput from "./components/FormInput/FormInput";
import JobType from "./components/JobType/JobType";
import MButton from "./components/MButton/MButton";

const App = () => {
  // ! Userlarni saqlash uchun
  const [users, setUsers] = useState([]);
  // ! Userlarni api-dan olish uchun
  const getUsers = () => {
    axios
      .get("https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/user")
      .then((res) => {
        setUsers(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // ! Component ishga tushganda userlarni olish
  useEffect(() => {
    getUsers();
  }, []);
  // ! Userni delete qilish uchun
  const deleteUser = (id) => {
    axios
      .delete(`https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/user/${id}`) // ! Hardoyim ham id bolib kelmaydi
      .then((res) => {
        getUsers();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // ! Formni dastlabki value-lari
  const intialValues = {
    user_infos: {
      firstName: '',
      lastName: '',
      email: '',
      phone_number: '',
      dob: ''
    },
    work_area: {
      company_name: '',
      job_type: '',
      experience: ''
    }
  };

  const [user_infos, setValues] = useState(intialValues.user_infos);
  const [work_area, setValues_work] = useState(intialValues.work_area);

  const clear = () => {
    setValues(intialValues.user_infos,intialValues.work_area);
  }

  // ! Formni submit qilish
  const onSubmit = (e) => {
    e.preventDefault();
    console.log("submit");


    const newValues = {
      user_infos,
      work_area

    // const formData = {
    //   // username: values.username,
    //   // email: values.email,
    //   // password: values.password,
    //   // confirmPassword: values.confirmPassword,
    //   ...values, // ! Osonro yoli lekin initialValues bilan backenddagi value-lari bir xil bo'lishi kerak (username === username)
    };
    // ! axios.post() yangi user qo'shish uchun
    axios
      .post("https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/user", newValues)
      .catch((err) => console.log("err", err))
      .finally(() => {
        getUsers();
        setValues(intialValues.user_infos);
        setValues_work(intialValues.work_area);

      });
  };

// job values
  const [jobs, setJobs] = useState([]);
  const getJobs = () => {
    axios
      .get("https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/types")
      .then((res) => {
        setJobs(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    getJobs();
  }, []);

  const clearJob = () => {
    setJobValues(intialJobValues);
  }

  const deleteJob = (id) => {
    axios
      .delete(`https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/types/${id}`)
      .then((res) => {
        getJobs();
      })
      .catch((err) => {
        console.log(err);
      });
  };


  const intialJobValues = {
    label: "",

  };
  const [jobValues, setJobValues] = useState(intialJobValues);


  const onSubmitJob = (e) => {
    e.preventDefault();
    console.log("submit");
    const formData = {
      ...jobValues,
    };




    axios
      .post("https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/types", formData)
      .catch((err) => console.log("err", err))
      .finally(() => {
        getJobs();
        setJobValues(intialJobValues);
      });
  };






  // // ! HINT FOR FUTURE:
  // const selectOptions = [
  //   { value: "", label: "Select..." },
  //   { value: "admin", label: "Admin" },
  //   { value: "user", label: "User" },
  // ];

  // const [selectedValue, setSelectedValue] = useState("");
  // console.log("selectedValue", selectedValue);

  return (
    <>
       <div className="info_cont">
        <div className="user_info1">
          <h1> User's Info</h1>
          <div className="underline"></div>
          <form className="form form_wrapping" id="form_wrapping" onSubmit={onSubmit}>

           <div className="info_forum">

           
            <FormInput
              required
              pattern="^[a-zA-Z0-9]{3,18}$"
              label="Name"
              name="firstName"
              placeholder="Enter your firstname"
              value={user_infos.firstName}
              handleChange={
                (e) => setValues({ ...user_infos, firstName: e.target.value})
              }
              errorMessage="Name is required"
            />
            <FormInput
              required
              pattern="^[a-zA-Z0-9]{3,18}$"
              label="Surname"
              name="lastName"
              placeholder="Enter your surname"
              value={user_infos.lastName}
              handleChange={
                (e) => setValues({ ...user_infos, lastName: e.target.value })
              }
              errorMessage="Surname is required"
            />
            <FormInput
              required
              type="number"
              label="Phone Number"
              name="phone_number"
              placeholder="Enter your phone number"
              value={user_infos.phone_number}
              handleChange={
                (e) => setValues({ ...user_infos, phone_number: e.target.value })
              }
              errorMessage="Phone Number is required"
            />
            <FormInput
              type="email"
              pattern={
                "^[a-zA-Z0-9.!#$%&'*+/=? ^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$"
              }
              errorMessage="Email is required"
              required
              label="Email"
              name="email"
              value={user_infos.email}
              placeholder="Enter your email"
              handleChange={(e) =>
                setValues({ ...user_infos, email: e.target.value })
              }
            />
            <FormInput
              type="date"
              label="Date of Birth"
              name="dob"
              required
              errorMessage="Date of birth is required"
              value={user_infos.dob}
              handleChange={(e) =>
                setValues({ ...user_infos, dob: e.target.value })
              }
            />
            </div>
          </form>
        </div>
        <div className="forms-mini">
          <div className="user_info2">
            <h1>Work Details</h1>
            <div className="underline"></div>
            <form className="form" onSubmit={onSubmit}>

              <FormInput
                required
                pattern="^[a-zA-Z0-9]{3,18}$"
                label="Company Name"
                name="company_name"
                value={work_area.company_name}
                placeholder="Enter company name"
                handleChange={
                  (e) => setValues_work({ ...work_area, company_name: e.target.value })
                }
                errorMessage="companyName is required"
              />
              <div className="select_flex">
                <div className="first_select">
                  <label>Job Type</label>
                  <select
                    name="job_type"

                    onChange={(e) => setValues_work({ ...work_area, job_type: e.target.value })}>
                    <option value="">Select job type</option>
                    {jobs.map((option) => (
                      <option key={option.id} value={option.label}>
                        {option.label}
                      </option>
                    ))}
                  </select>
                </div>
                <div className="second_select">
                  <label>Experience</label>
                  <select
                    name="experience"
                    onChange={(e) =>
                      setValues_work({ ...work_area, experience: e.target.value })
                    }

                  >
                    <option value="">Select experience</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </select>
                </div>
              </div>
            </form>
          </div>
          <div className="user_info3">
            <h1>Job type</h1>
            <div className="line"></div>
            <div className="underline"></div>
            <form className="form" onSubmit={onSubmit}>

              <FormInput
                required
                pattern="^[a-zA-Z0-9]{3,18}$"
                label="Label"
                name="jobTypeEnter"
                placeholder="Enter job type ..."
                value={jobValues.label}
                handleChange={
                  (e) => setJobValues({ ...jobValues, label: e.target.value })
                }
                errorMessage="Enter job..."
              />
              <div className="button_flex">
              <MButton 
                BSize={"submit"}
                onClick={clearJob}
                BStyle={{ marginBottom: "15px", width: "20%" , marginRight: "20px"}}
                BType={"outline-primary"}
                >
                Отменить
              </MButton>
          
          <MButton 
            BSize={"submit"}
            onClick={onSubmitJob}
            BStyle={{ marginBottom: "15px", width: "20%"}}
            type="submit"
          >
            Сохранить
          </MButton>
          </div>
            </form>
          </div>
          <div className="button_flex">
            
          <MButton 
                BSize={"submit"}
                onClick={clear}
                BStyle={{ marginBottom: "15px", width: "20%" , marginRight: "20px"}}
                BType={"outline-primary"}
                >
                Отменить
          </MButton>

          <MButton 
            BSize={"submit"}
            onClick={onSubmit}
            BStyle={{ marginBottom: "15px", marginRight: "15px" , width: "20%"}}
            type="submit"
          >
            Сохранить
          </MButton>
          </div>
        </div>

      </div>

      {/* // ! Userlist */}
      <UserList users={users} deleteUser={deleteUser} /> 
      <JobType jobs={jobs} deleteJob={deleteJob} />
      {/* <select
        style={{ margin: "10px 60px", padding: "10px", minWidth: "200px" }}
        onChange={(e) => setSelectedValue(e.target.value)}
      > */}
        {/* // ! Backenddan axios orqali get qilib olgan dataniyam optionlarda aylansa bo'ladi */}
        {/* {selectOptions.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </select> */}
    </>
  );
};

export default App;
