import React from "react";
import { ReactComponent as DeleteIcon } from "../../Assets/Images/delete.svg";
import MButton from "../MButton/MButton";
import "./userList.css";

const UserList = ({ users, deleteUser }) => {
  return (
    <div className="userList">
      <h1>Users</h1>
      <div className="underline"></div>

      <div className="line"></div>
      <div className="static default">
      <div className="item">№</div>
        <div className="item">Full name</div>
        <div className="item">Date of Birth</div>
        <div className="item">Phone</div>
        <div className="item item-email">Email</div>
        <div className="item">Company Name</div>
        <div className="item">Selected Job</div>
        <div className="item">Experience</div>
        <div className="item"></div>
      </div>
      {users &&
        users.map((user) => (
          <div className="default" key={user?.id}>
             <div className="item">{user?.id}</div>
            <div className="item">{user?.user_infos?.firstName} {user?.user_infos?.lastName}</div>
            <div className="item">{user?.user_infos?.dob}</div>
            <div className="item">{user?.user_infos?.phone_number}</div>
            <div className="item item-email">{user?.user_infos?.email}</div>
            <div className="item">{user?.work_area?.company_name}</div>
            <div className="item">{user?.work_area?.job_type}</div>
            <div className="item">{user?.work_area?.experience}</div>
            <MButton
              onClick={() => {
                console.log('delete user',user?.id);
                deleteUser(user?.id)
              }}
              BClass="item action action__delete"
              BType={"outline-primary"}
              BIcon={<DeleteIcon />}
            />
          </div>
        ))}
    </div>
  );
};

export default UserList;
